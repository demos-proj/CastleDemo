﻿using System;

namespace _01CastleBasic
{
    /// <summary>
    /// 编写：Terrylee
    /// 出处：http://terrylee.cnblogs.com
    /// </summary>
    internal class TextFileLog : ILog
    {
        private string _target;
        private ILogFormatter _format;

        public TextFileLog(string target, ILogFormatter format)
        {
            this._target = target;
            this._format = format;
        }

        public void Write(string MsgStr)
        {
            string _MsgStr = _format.Format(MsgStr);
            _MsgStr += _target;

            // Output Message
            Console.WriteLine("Output " + _MsgStr);
        }
    }
}