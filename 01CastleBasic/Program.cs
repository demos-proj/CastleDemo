﻿using System;
using Castle.MicroKernel;
using Castle.MicroKernel.Registration;
using Castle.Windsor;

namespace _01CastleBasic
{
    internal partial class Program
    {
        private static void Main(string[] args)
        {
            // 1、注册
            // for+接口
            // implementedBy+实现类
            IWindsorContainer container = new WindsorContainer();
            container.Register(Component.For<ILog>().ImplementedBy<TextFileLog>());
            container.Register(Component.For<ILogFormatter>().ImplementedBy<TextFormatter>());

            // 2、调用 （无参、有参）
            // ILogFormatter 对应实现类“构造函数无参”，直接Resolve即可
            // Ilog 对应的实现类“构造函数有参”，需要resolve+arguments
            ILog log = container.Resolve<ILog>(new Arguments()
            {
                {"target","1.txt"},
                {"format", container.Resolve<ILogFormatter>()},
            });
            log.Write("hello world");
            Console.WriteLine("按任意键继续");
            Console.ReadKey();
        }
    }
}