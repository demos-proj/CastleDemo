﻿namespace _01CastleBasic
{
    /// <summary>
    /// 编写：Terrylee
    /// 出处：http://terrylee.cnblogs.com
    /// </summary>
    internal class TextFormatter : ILogFormatter
    {
        public TextFormatter()
        {
        }

        public string Format(string MsgStr)
        {
            return "[" + MsgStr + "]";
        }
    }
}