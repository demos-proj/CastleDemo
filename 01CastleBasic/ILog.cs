﻿namespace _01CastleBasic
{
    /// <summary>
    /// 编写：Terrylee
    /// 出处：http://terrylee.cnblogs.com
    /// </summary>
    internal interface ILog
    {
        void Write(string MsgStr);
    }
}