﻿using Castle.DynamicProxy;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CastleInterceptorDemo
{

    public class MyInterceptorStopwatch : IInterceptor, IMyInterceptorStopwatch
    {
        public void Intercept(IInvocation invocation)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            invocation.Proceed();
            sw.Stop();
            Console.WriteLine(invocation.GetConcreteMethod() + "用时:" + sw.Elapsed);
        }
    }

    public interface IMyInterceptorStopwatch
    {

    }

}
