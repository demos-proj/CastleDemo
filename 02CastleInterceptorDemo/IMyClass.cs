﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CastleInterceptorDemo
{

    public interface IMyClass
    {
        void MethodInterceptor();
        void NoInterceptor();
    }
}
