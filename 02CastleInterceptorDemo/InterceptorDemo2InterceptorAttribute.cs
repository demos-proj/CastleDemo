﻿using Castle.Core;
using Castle.DynamicProxy;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CastleInterceptorDemo
{

    public class InterceptorDemo2InterceptorAttribute
    {
        [Interceptor(typeof(IMyInterceptor))]
        [Interceptor(typeof(IMyInterceptorStopwatch))]
        public class MyClass : IMyClass
        {
            public virtual void MethodInterceptor()
            {
                Console.WriteLine("走过滤器");
            }

            public void NoInterceptor()
            {
                Console.WriteLine("没有走过滤器");
            }
        }
        public void Run()
        {
            var container = new WindsorContainer();
            container.Register(Component.For<IMyInterceptor>().ImplementedBy<MyInterceptor>());
            container.Register(Component.For<IMyInterceptorStopwatch>().ImplementedBy<MyInterceptorStopwatch>());
            container.Register(Component.For<IMyClass>().ImplementedBy<MyClass>());
            var myClass = container.Resolve<IMyClass>();
            Console.WriteLine("当前类型:{0},父类型:{1}", myClass.GetType(), myClass.GetType().BaseType);
            Console.WriteLine();
            myClass.MethodInterceptor();
            Console.WriteLine();
            myClass.NoInterceptor();
            Console.WriteLine();
            Console.ReadLine();
        }
    }
}
