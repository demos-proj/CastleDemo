﻿using Castle.Core;
using Castle.DynamicProxy;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CastleInterceptorDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            // 通过ProxyGenerator代理生成器：
            // 自定义类
            // 自定义拦截器类
            // ProxyGenerator.CreateClassProxy<自定义类>(自定义拦截器类);
            new InterceptorDemo1ProxyGenerator().Run();
            // 通过InterceptorAttribute特性
            // 自定义类（+自定义类接口）+InterceptorAttribute（+拦截器接口）
            // 自定义拦截器类+拦截器接口
            // container.Resolve<IMyClass>()
            //new InterceptorDemo2().Run();
        }
    }
}
