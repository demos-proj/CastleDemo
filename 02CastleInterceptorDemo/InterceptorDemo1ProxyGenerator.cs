﻿using Castle.DynamicProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CastleInterceptorDemo
{

    public class InterceptorDemo1ProxyGenerator
    {
        public class MyClass
        {
            public virtual void MethodInterceptor()
            {
                Console.WriteLine("走过滤器");
            }

            public void NoInterceptor()
            {
                Console.WriteLine("没有走过滤器");
            }
        }

        public void Run()
        {
            ProxyGenerator generator = new ProxyGenerator();//实例化【代理类生成器】  
            var myClass = generator.CreateClassProxy<MyClass>(new MyInterceptor(), new MyInterceptorStopwatch());
            Console.WriteLine("当前类型:{0},父类型:{1}", myClass.GetType(), myClass.GetType().BaseType);
            Console.WriteLine();
            myClass.MethodInterceptor();
            Console.WriteLine();
            myClass.NoInterceptor();
            Console.WriteLine();
            Console.ReadLine();
        }
    }
}
